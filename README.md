# Failure Path CI

## Continuous Integration: 
* Modify **.gitlab-ci.yml** to adapt to docker.
* Commit - push the code to gitlab
* If no error, you'll have a green tick, if not, click on the error sign and follow it to see the error code
